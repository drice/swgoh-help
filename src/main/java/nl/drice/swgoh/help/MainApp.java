package nl.drice.swgoh.help;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.google.gson.Gson;

import help.swgoh.api.SwgohAPI;
import help.swgoh.api.SwgohAPIBuilder;
import nl.drice.swgoh.help.model.ToolPlayer;
import nl.drice.swgoh.help.model.guild.Guild;
import nl.drice.swgoh.help.model.guild.Roster;
import nl.drice.swgoh.help.model.player.Mod;
import nl.drice.swgoh.help.model.player.Player;
import nl.drice.swgoh.help.model.player.SecondaryStat;
import nl.drice.swgoh.help.model.player.Skill;

public class MainApp {

	final static int[] allyCodelist = { 859197191 };
	final static int[] losseMembers = { 883573521, 724966641, 643458792, 658799425, 749573533, 866268745, 317824548,
			174628129, 267853664, 827865198, 889667644, 986621568, 548521177, 315895725 };
	List<ToolPlayer> players = new ArrayList<ToolPlayer>();

//Name	GP	Arena 	Fleet Zetas	TW NS	TW CLS	TW GG	TW bugs	6* mods	mods >= 20	R7	R6	R4/5	R1-R3
	public static void main(String[] args) {
		MainApp app = new MainApp();
		app.run();

	}

	public void run() {
		SwgohAPI api = new SwgohAPIBuilder().withUsername("Drice").withPassword("vFD5W2g7McrJyj2d").build();

		for (int allycode : allyCodelist) {

			try {
				Guild guild = getGuild(api, allycode);
				List<Roster> members = guild.getRoster();

				for (Roster member : members) {
					String playerJson = api.getPlayer(member.getAllyCode()).get();
					Player[] playerModel = new Gson().fromJson(playerJson, Player[].class);
					for (Player player : playerModel) {
						processPlayer(player);
					}
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		
		for(int memberCode: losseMembers) {
			try {
				String playerJson = api.getPlayer(memberCode).get();
				Player[] playerModel = new Gson().fromJson(playerJson, Player[].class);
				for (Player player : playerModel) {
					processPlayer(player);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			
		}
		
		writePlayersToCSV(players);
	}

	private void processPlayer(Player apiplayer) {

		ToolPlayer player = new ToolPlayer(apiplayer.getAllyCode());
		player.setName(apiplayer.getName());
		player.setGuild(apiplayer.getGuildName());
		player.setArenaRank(apiplayer.getArena().getChar().getRank());
		player.setShipRank(apiplayer.getArena().getShip().getRank());
		int zetas = 0;
		int speedmods = 0;
		int sixmods = 0;
		int r1 = 0;
		int r2 = 0;
		int r3 = 0;
		int r4 = 0;
		int r5 = 0;
		int r6 = 0;
		int r7 = 0;

		List<nl.drice.swgoh.help.model.player.Roster> roster = apiplayer.getRoster();
		for (nl.drice.swgoh.help.model.player.Roster character : roster) {

			if (character.getCombatType() == 1) {
				if (character.getGear() == 13) {
					int relic = character.getRelic().getCurrentTier();
					relic = relic - 2;
					if (relic < 0) {
						relic = 0;
					}
					switch (relic) {
					case 1:
						r1++;
						break;
					case 2:
						r2++;
						break;
					case 3:
						r3++;
						break;
					case 4:
						r4++;
						break;
					case 5:
						r5++;
						break;
					case 6:
						r6++;
						break;
					case 7:
						r7++;
						break;

					}
				}
			}
			// mods section
			List<Mod> mods = character.getMods();
			for (Mod mod : mods) {
				if (mod.getPips() == 6) {
					sixmods++;
				}
				List<SecondaryStat> stats = mod.getSecondaryStat();
				for (SecondaryStat stat : stats) {
					if ((stat.getUnitStat() == 5) && stat.getValue() >= 20) {
						speedmods++;
					}
				}
			}

			// zetas section
			List<Skill> skills = character.getSkills();
			for (Skill skill : skills) {
				if (skill.getIsZeta()) {
					if (skill.getTier() == skill.getTiers()) {
						zetas++;
					}
				}
			}

		}
		player.setMod6Count(sixmods);
		player.setSpeed20ModCount(speedmods);
		player.setZetaCount(zetas);
		player.setR1(r1);
		player.setR2(r2);
		player.setR3(r3);
		player.setR4(r4);
		player.setR5(r5);
		player.setR6(r6);
		player.setR7(r7);
		players.add(player);
	}

	private Guild getGuild(SwgohAPI api, int allycode) throws InterruptedException, ExecutionException {
		String guildJson = api.getLargeGuild(allycode).get();
		Guild[] guildModel = new Gson().fromJson(guildJson, Guild[].class);
		Guild guild = guildModel[0];
		return guild;
	}

	public void writePlayersToCSV(List<ToolPlayer> players) {
		Writer writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("tooldata.csv"), "windows-1252"));
			// GP Arena Fleet Zetas TW NS TW CLS TW GG TW bugs 6* mods mods >= 20 R7 R6 R4/5
			// R1-R3
			writer.write("allycode,player,guild,gp,arena,ship,6mods,speedmods,r7,r6,r5,r4,r3,r2,r1");
			writer.write("\n");
			for (ToolPlayer player : players) {
				writePlayerToWriter(player, writer);
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (Exception ex) {
				/* ignore */}
		}
	}

	private void writePlayerToWriter(ToolPlayer player, Writer writer) throws IOException {
		StringBuilder builder = new StringBuilder();
		builder.append(player.getAllycode()).append(",");
		builder.append(player.getName()).append(",");
		builder.append(player.getGuild()).append(",");
		builder.append(0).append(",");
		builder.append(player.getArenaRank()).append(",");
		builder.append(player.getShipRank()).append(",");
		builder.append(player.getMod6Count()).append(",");
		builder.append(player.getSpeed20ModCount()).append(",");
		builder.append(player.getR7()).append(",");
		builder.append(player.getR6()).append(",");
		builder.append(player.getR5()).append(",");
		builder.append(player.getR4()).append(",");
		builder.append(player.getR3()).append(",");
		builder.append(player.getR2()).append(",");
		builder.append(player.getR1());

		writer.write(builder.toString());
		writer.write("\n");
	}

}
