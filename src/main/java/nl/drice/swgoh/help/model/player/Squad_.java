package nl.drice.swgoh.help.model.player;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Squad_ {

@SerializedName("id")
@Expose
private String id;
@SerializedName("defId")
@Expose
private String defId;
@SerializedName("squadUnitType")
@Expose
private Integer squadUnitType;

public String getId() {
return id;
 }

public void setId(String id) {
this.id = id;
 }

public String getDefId() {
return defId;
 }

public void setDefId(String defId) {
this.defId = defId;
 }

public Integer getSquadUnitType() {
return squadUnitType;
 }

public void setSquadUnitType(Integer squadUnitType) {
this.squadUnitType = squadUnitType;
 }

}