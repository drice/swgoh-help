
package nl.drice.swgoh.help.model.player;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Roster {

@SerializedName("id")
@Expose
private String id;
@SerializedName("defId")
@Expose
private String defId;
@SerializedName("nameKey")
@Expose
private String nameKey;
@SerializedName("rarity")
@Expose
private Integer rarity;
@SerializedName("level")
@Expose
private Integer level;
@SerializedName("xp")
@Expose
private Integer xp;
@SerializedName("gear")
@Expose
private Integer gear;
@SerializedName("equipped")
@Expose
private List<Equipped> equipped = null;
@SerializedName("combatType")
@Expose
private Integer combatType;
@SerializedName("skills")
@Expose
private List<Skill> skills = null;
@SerializedName("mods")
@Expose
private List<Mod> mods = null;
@SerializedName("crew")
@Expose
private List<Object> crew = null;
@SerializedName("gp")
@Expose
private Integer gp;
@SerializedName("primaryUnitStat")
@Expose
private Object primaryUnitStat;
@SerializedName("relic")
@Expose
private Relic relic;

public String getId() {
return id;
 }

public void setId(String id) {
this.id = id;
 }

public String getDefId() {
return defId;
 }

public void setDefId(String defId) {
this.defId = defId;
 }

public String getNameKey() {
return nameKey;
 }

public void setNameKey(String nameKey) {
this.nameKey = nameKey;
 }

public Integer getRarity() {
return rarity;
 }

public void setRarity(Integer rarity) {
this.rarity = rarity;
 }

public Integer getLevel() {
return level;
 }

public void setLevel(Integer level) {
this.level = level;
 }

public Integer getXp() {
return xp;
 }

public void setXp(Integer xp) {
this.xp = xp;
 }

public Integer getGear() {
return gear;
 }

public void setGear(Integer gear) {
this.gear = gear;
 }

public List<Equipped> getEquipped() {
return equipped;
 }

public void setEquipped(List<Equipped> equipped) {
this.equipped = equipped;
 }

public Integer getCombatType() {
return combatType;
 }

public void setCombatType(Integer combatType) {
this.combatType = combatType;
 }

public List<Skill> getSkills() {
return skills;
 }

public void setSkills(List<Skill> skills) {
this.skills = skills;
 }

public List<Mod> getMods() {
return mods;
 }

public void setMods(List<Mod> mods) {
this.mods = mods;
 }

public List<Object> getCrew() {
return crew;
 }

public void setCrew(List<Object> crew) {
this.crew = crew;
 }

public Integer getGp() {
return gp;
 }

public void setGp(Integer gp) {
this.gp = gp;
 }

public Object getPrimaryUnitStat() {
return primaryUnitStat;
 }

public void setPrimaryUnitStat(Object primaryUnitStat) {
this.primaryUnitStat = primaryUnitStat;
 }

public Relic getRelic() {
return relic;
 }

public void setRelic(Relic relic) {
this.relic = relic;
 }

}