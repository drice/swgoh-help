package nl.drice.swgoh.help.model.guild;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Raid {

	@SerializedName("rancor")
	@Expose
	private String rancor;
	@SerializedName("aat")
	@Expose
	private String aat;
	@SerializedName("sith_raid")
	@Expose
	private String sithRaid;

	public String getRancor() {
		return rancor;
	}

	public void setRancor(String rancor) {
		this.rancor = rancor;
	}

	public String getAat() {
		return aat;
	}

	public void setAat(String aat) {
		this.aat = aat;
	}

	public String getSithRaid() {
		return sithRaid;
	}

	public void setSithRaid(String sithRaid) {
		this.sithRaid = sithRaid;
	}

}
