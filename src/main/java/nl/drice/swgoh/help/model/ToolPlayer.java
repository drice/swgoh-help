package nl.drice.swgoh.help.model;

public class ToolPlayer {
	
	private int allycode;
	private String name;
	private String guild;
	private Long charGP;
	private Long shipGP;
	private int zetaCount;
	private int mod6Count;
	private int speed20ModCount;
	private int r1;
	private int r2;
	private int r3;
	private int r4;
	private int r5;
	private int r6;
	private int r7;
	private int arenaRank;
	private int shipRank;
	
	public ToolPlayer(int allycode) {
		this.allycode = allycode;
	}
	
	public int getAllycode() {
		return allycode;
	}
	public void setAllycode(int allycode) {
		this.allycode = allycode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGuild() {
		return guild;
	}
	public void setGuild(String guild) {
		this.guild = guild;
	}
	public Long getCharGP() {
		return charGP;
	}
	public void setCharGP(Long charGP) {
		this.charGP = charGP;
	}
	public Long getShipGP() {
		return shipGP;
	}
	public void setShipGP(Long shipGP) {
		this.shipGP = shipGP;
	}
	public int getZetaCount() {
		return zetaCount;
	}
	public void setZetaCount(int zetaCount) {
		this.zetaCount = zetaCount;
	}
	public int getMod6Count() {
		return mod6Count;
	}
	public void setMod6Count(int mod6Count) {
		this.mod6Count = mod6Count;
	}
	public int getSpeed20ModCount() {
		return speed20ModCount;
	}
	public void setSpeed20ModCount(int speed20ModCount) {
		this.speed20ModCount = speed20ModCount;
	}
	public int getR1() {
		return r1;
	}
	public void setR1(int r1) {
		this.r1 = r1;
	}
	public int getR2() {
		return r2;
	}
	public void setR2(int r2) {
		this.r2 = r2;
	}
	public int getR3() {
		return r3;
	}
	public void setR3(int r3) {
		this.r3 = r3;
	}
	public int getR4() {
		return r4;
	}
	public void setR4(int r4) {
		this.r4 = r4;
	}
	public int getR5() {
		return r5;
	}
	public void setR5(int r5) {
		this.r5 = r5;
	}
	public int getR6() {
		return r6;
	}
	public void setR6(int r6) {
		this.r6 = r6;
	}
	public int getR7() {
		return r7;
	}
	public void setR7(int r7) {
		this.r7 = r7;
	}
	public int getArenaRank() {
		return arenaRank;
	}
	public void setArenaRank(int arenaRank) {
		this.arenaRank = arenaRank;
	}
	public int getShipRank() {
		return shipRank;
	}
	public void setShipRank(int shipRank) {
		this.shipRank = shipRank;
	}
	

}
