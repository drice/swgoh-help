package nl.drice.swgoh.help.model.player;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Portraits {

	@SerializedName("selected")
	@Expose
	private String selected;
	@SerializedName("unlocked")
	@Expose
	private List<String> unlocked = null;

	public String getSelected() {
		return selected;
	}

	public void setSelected(String selected) {
		this.selected = selected;
	}

	public List<String> getUnlocked() {
		return unlocked;
	}

	public void setUnlocked(List<String> unlocked) {
		this.unlocked = unlocked;
	}

}