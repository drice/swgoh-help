package nl.drice.swgoh.help.model.player;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrimaryStat {

	@SerializedName("unitStat")
	@Expose
	private Integer unitStat;
	@SerializedName("value")
	@Expose
	private Double value;

	public Integer getUnitStat() {
		return unitStat;
	}

	public void setUnitStat(Integer unitStat) {
		this.unitStat = unitStat;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

}