package nl.drice.swgoh.help.model.player;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Skill {

@SerializedName("id")
@Expose
private String id;
@SerializedName("tier")
@Expose
private Integer tier;
@SerializedName("nameKey")
@Expose
private String nameKey;
@SerializedName("isZeta")
@Expose
private Boolean isZeta;
@SerializedName("tiers")
@Expose
private Integer tiers;

public String getId() {
return id;
 }

public void setId(String id) {
this.id = id;
 }

public Integer getTier() {
return tier;
 }

public void setTier(Integer tier) {
this.tier = tier;
 }

public String getNameKey() {
return nameKey;
 }

public void setNameKey(String nameKey) {
this.nameKey = nameKey;
 }

public Boolean getIsZeta() {
return isZeta;
 }

public void setIsZeta(Boolean isZeta) {
this.isZeta = isZeta;
 }

public Integer getTiers() {
return tiers;
 }

public void setTiers(Integer tiers) {
this.tiers = tiers;
 }

}