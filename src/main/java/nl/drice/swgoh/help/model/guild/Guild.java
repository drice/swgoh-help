package nl.drice.swgoh.help.model.guild;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Guild {

	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("desc")
	@Expose
	private String desc;
	@SerializedName("members")
	@Expose
	private Integer members;
	@SerializedName("status")
	@Expose
	private Integer status;
	@SerializedName("required")
	@Expose
	private Integer required;
	@SerializedName("bannerColor")
	@Expose
	private String bannerColor;
	@SerializedName("bannerLogo")
	@Expose
	private String bannerLogo;
	@SerializedName("message")
	@Expose
	private String message;
	@SerializedName("gp")
	@Expose
	private Integer gp;
	@SerializedName("raid")
	@Expose
	private Raid raid;
	@SerializedName("roster")
	@Expose
	private List<Roster> roster = null;
	@SerializedName("updated")
	@Expose
	private Long updated;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getMembers() {
		return members;
	}

	public void setMembers(Integer members) {
		this.members = members;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getRequired() {
		return required;
	}

	public void setRequired(Integer required) {
		this.required = required;
	}

	public String getBannerColor() {
		return bannerColor;
	}

	public void setBannerColor(String bannerColor) {
		this.bannerColor = bannerColor;
	}

	public String getBannerLogo() {
		return bannerLogo;
	}

	public void setBannerLogo(String bannerLogo) {
		this.bannerLogo = bannerLogo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getGp() {
		return gp;
	}

	public void setGp(Integer gp) {
		this.gp = gp;
	}

	public Raid getRaid() {
		return raid;
	}

	public void setRaid(Raid raid) {
		this.raid = raid;
	}

	public List<Roster> getRoster() {
		return roster;
	}

	public void setRoster(List<Roster> roster) {
		this.roster = roster;
	}

	public Long getUpdated() {
		return updated;
	}

	public void setUpdated(Long updated) {
		this.updated = updated;
	}

}
