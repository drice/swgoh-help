
package nl.drice.swgoh.help.model.player;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Mod {

	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("level")
	@Expose
	private Integer level;
	@SerializedName("tier")
	@Expose
	private Integer tier;
	@SerializedName("slot")
	@Expose
	private Integer slot;
	@SerializedName("set")
	@Expose
	private Integer set;
	@SerializedName("pips")
	@Expose
	private Integer pips;
	@SerializedName("primaryStat")
	@Expose
	private PrimaryStat primaryStat;
	@SerializedName("secondaryStat")
	@Expose
	private List<SecondaryStat> secondaryStat = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getTier() {
		return tier;
	}

	public void setTier(Integer tier) {
		this.tier = tier;
	}

	public Integer getSlot() {
		return slot;
	}

	public void setSlot(Integer slot) {
		this.slot = slot;
	}

	public Integer getSet() {
		return set;
	}

	public void setSet(Integer set) {
		this.set = set;
	}

	public Integer getPips() {
		return pips;
	}

	public void setPips(Integer pips) {
		this.pips = pips;
	}

	public PrimaryStat getPrimaryStat() {
		return primaryStat;
	}

	public void setPrimaryStat(PrimaryStat primaryStat) {
		this.primaryStat = primaryStat;
	}

	public List<SecondaryStat> getSecondaryStat() {
		return secondaryStat;
	}

	public void setSecondaryStat(List<SecondaryStat> secondaryStat) {
		this.secondaryStat = secondaryStat;
	}

}