package nl.drice.swgoh.help.model.player;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Arena {

	@SerializedName("char")
	@Expose
	private Char _char;
	@SerializedName("ship")
	@Expose
	private Ship ship;

	public Char getChar() {
		return _char;
	}

	public void setChar(Char _char) {
		this._char = _char;
	}

	public Ship getShip() {
		return ship;
	}

	public void setShip(Ship ship) {
		this.ship = ship;
	}

}