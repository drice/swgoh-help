package nl.drice.swgoh.help.model.player;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SecondaryStat {

@SerializedName("unitStat")
@Expose
private Integer unitStat;
@SerializedName("value")
@Expose
private Double value;
@SerializedName("roll")
@Expose
private Integer roll;

public Integer getUnitStat() {
return unitStat;
 }

public void setUnitStat(Integer unitStat) {
this.unitStat = unitStat;
 }

public Double getValue() {
return value;
 }

public void setValue(Double value) {
this.value = value;
 }

public Integer getRoll() {
return roll;
 }

public void setRoll(Integer roll) {
this.roll = roll;
 }

}