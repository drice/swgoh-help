package nl.drice.swgoh.help.model.guild;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Roster {

	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("guildMemberLevel")
	@Expose
	private Integer guildMemberLevel;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("level")
	@Expose
	private Integer level;
	@SerializedName("allyCode")
	@Expose
	private Integer allyCode;
	@SerializedName("gp")
	@Expose
	private Integer gp;
	@SerializedName("gpChar")
	@Expose
	private Integer gpChar;
	@SerializedName("gpShip")
	@Expose
	private Integer gpShip;
	@SerializedName("updated")
	@Expose
	private Long updated;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getGuildMemberLevel() {
		return guildMemberLevel;
	}

	public void setGuildMemberLevel(Integer guildMemberLevel) {
		this.guildMemberLevel = guildMemberLevel;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getAllyCode() {
		return allyCode;
	}

	public void setAllyCode(Integer allyCode) {
		this.allyCode = allyCode;
	}

	public Integer getGp() {
		return gp;
	}

	public void setGp(Integer gp) {
		this.gp = gp;
	}

	public Integer getGpChar() {
		return gpChar;
	}

	public void setGpChar(Integer gpChar) {
		this.gpChar = gpChar;
	}

	public Integer getGpShip() {
		return gpShip;
	}

	public void setGpShip(Integer gpShip) {
		this.gpShip = gpShip;
	}

	public Long getUpdated() {
		return updated;
	}

	public void setUpdated(Long updated) {
		this.updated = updated;
	}

}
