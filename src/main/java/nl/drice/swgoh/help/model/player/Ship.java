package nl.drice.swgoh.help.model.player;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ship {

@SerializedName("rank")
@Expose
private Integer rank;
@SerializedName("squad")
@Expose
private List<Squad_> squad = null;

public Integer getRank() {
return rank;
 }

public void setRank(Integer rank) {
this.rank = rank;
 }

public List<Squad_> getSquad() {
return squad;
 }

public void setSquad(List<Squad_> squad) {
this.squad = squad;
 }

}