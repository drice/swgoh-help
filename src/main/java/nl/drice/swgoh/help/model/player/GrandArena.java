package nl.drice.swgoh.help.model.player;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GrandArena {

	@SerializedName("seasonId")
	@Expose
	private String seasonId;
	@SerializedName("eventInstanceId")
	@Expose
	private String eventInstanceId;
	@SerializedName("league")
	@Expose
	private String league;
	@SerializedName("wins")
	@Expose
	private Integer wins;
	@SerializedName("losses")
	@Expose
	private Integer losses;
	@SerializedName("eliteDivision")
	@Expose
	private Boolean eliteDivision;
	@SerializedName("seasonPoints")
	@Expose
	private Integer seasonPoints;
	@SerializedName("division")
	@Expose
	private Integer division;
	@SerializedName("remove")
	@Expose
	private Boolean remove;
	@SerializedName("rank")
	@Expose
	private Integer rank;

	public String getSeasonId() {
		return seasonId;
	}

	public void setSeasonId(String seasonId) {
		this.seasonId = seasonId;
	}

	public String getEventInstanceId() {
		return eventInstanceId;
	}

	public void setEventInstanceId(String eventInstanceId) {
		this.eventInstanceId = eventInstanceId;
	}

	public String getLeague() {
		return league;
	}

	public void setLeague(String league) {
		this.league = league;
	}

	public Integer getWins() {
		return wins;
	}

	public void setWins(Integer wins) {
		this.wins = wins;
	}

	public Integer getLosses() {
		return losses;
	}

	public void setLosses(Integer losses) {
		this.losses = losses;
	}

	public Boolean getEliteDivision() {
		return eliteDivision;
	}

	public void setEliteDivision(Boolean eliteDivision) {
		this.eliteDivision = eliteDivision;
	}

	public Integer getSeasonPoints() {
		return seasonPoints;
	}

	public void setSeasonPoints(Integer seasonPoints) {
		this.seasonPoints = seasonPoints;
	}

	public Integer getDivision() {
		return division;
	}

	public void setDivision(Integer division) {
		this.division = division;
	}

	public Boolean getRemove() {
		return remove;
	}

	public void setRemove(Boolean remove) {
		this.remove = remove;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

}