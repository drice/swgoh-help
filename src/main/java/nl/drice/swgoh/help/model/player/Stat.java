package nl.drice.swgoh.help.model.player;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stat {

	@SerializedName("nameKey")
	@Expose
	private String nameKey;
	@SerializedName("value")
	@Expose
	private Long value;
	@SerializedName("index")
	@Expose
	private Integer index;

	public String getNameKey() {
		return nameKey;
	}

	public void setNameKey(String nameKey) {
		this.nameKey = nameKey;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

}