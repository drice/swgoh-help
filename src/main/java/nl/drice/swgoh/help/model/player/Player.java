package nl.drice.swgoh.help.model.player;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Player {

	@SerializedName("allyCode")
	@Expose
	private Integer allyCode;
	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("level")
	@Expose
	private Integer level;
	@SerializedName("titles")
	@Expose
	private Titles titles;
	@SerializedName("guildRefId")
	@Expose
	private String guildRefId;
	@SerializedName("guildName")
	@Expose
	private String guildName;
	@SerializedName("guildBannerColor")
	@Expose
	private String guildBannerColor;
	@SerializedName("guildBannerLogo")
	@Expose
	private String guildBannerLogo;
	@SerializedName("guildTypeId")
	@Expose
	private String guildTypeId;
	@SerializedName("stats")
	@Expose
	private List<Stat> stats = null;
	@SerializedName("roster")
	@Expose
	private List<Roster> roster = null;
	@SerializedName("arena")
	@Expose
	private Arena arena;
	@SerializedName("lastActivity")
	@Expose
	private Long lastActivity;
	@SerializedName("poUTCOffsetMinutes")
	@Expose
	private Integer poUTCOffsetMinutes;
	@SerializedName("portraits")
	@Expose
	private Portraits portraits;
	@SerializedName("grandArena")
	@Expose
	private List<GrandArena> grandArena = null;
	@SerializedName("grandArenaLifeTime")
	@Expose
	private Integer grandArenaLifeTime;
	@SerializedName("updated")
	@Expose
	private Long updated;

	public Integer getAllyCode() {
		return allyCode;
	}

	public void setAllyCode(Integer allyCode) {
		this.allyCode = allyCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Titles getTitles() {
		return titles;
	}

	public void setTitles(Titles titles) {
		this.titles = titles;
	}

	public String getGuildRefId() {
		return guildRefId;
	}

	public void setGuildRefId(String guildRefId) {
		this.guildRefId = guildRefId;
	}

	public String getGuildName() {
		return guildName;
	}

	public void setGuildName(String guildName) {
		this.guildName = guildName;
	}

	public String getGuildBannerColor() {
		return guildBannerColor;
	}

	public void setGuildBannerColor(String guildBannerColor) {
		this.guildBannerColor = guildBannerColor;
	}

	public String getGuildBannerLogo() {
		return guildBannerLogo;
	}

	public void setGuildBannerLogo(String guildBannerLogo) {
		this.guildBannerLogo = guildBannerLogo;
	}

	public String getGuildTypeId() {
		return guildTypeId;
	}

	public void setGuildTypeId(String guildTypeId) {
		this.guildTypeId = guildTypeId;
	}

	public List<Stat> getStats() {
		return stats;
	}

	public void setStats(List<Stat> stats) {
		this.stats = stats;
	}

	public List<Roster> getRoster() {
		return roster;
	}

	public void setRoster(List<Roster> roster) {
		this.roster = roster;
	}

	public Arena getArena() {
		return arena;
	}

	public void setArena(Arena arena) {
		this.arena = arena;
	}

	public Long getLastActivity() {
		return lastActivity;
	}

	public void setLastActivity(Long lastActivity) {
		this.lastActivity = lastActivity;
	}

	public Integer getPoUTCOffsetMinutes() {
		return poUTCOffsetMinutes;
	}

	public void setPoUTCOffsetMinutes(Integer poUTCOffsetMinutes) {
		this.poUTCOffsetMinutes = poUTCOffsetMinutes;
	}

	public Portraits getPortraits() {
		return portraits;
	}

	public void setPortraits(Portraits portraits) {
		this.portraits = portraits;
	}

	public List<GrandArena> getGrandArena() {
		return grandArena;
	}

	public void setGrandArena(List<GrandArena> grandArena) {
		this.grandArena = grandArena;
	}

	public Integer getGrandArenaLifeTime() {
		return grandArenaLifeTime;
	}

	public void setGrandArenaLifeTime(Integer grandArenaLifeTime) {
		this.grandArenaLifeTime = grandArenaLifeTime;
	}

	public Long getUpdated() {
		return updated;
	}

	public void setUpdated(Long updated) {
		this.updated = updated;
	}

}