package nl.drice.swgoh.help.model.player;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Relic {

@SerializedName("currentTier")
@Expose
private Integer currentTier;

public Integer getCurrentTier() {
return currentTier;
 }

public void setCurrentTier(Integer currentTier) {
this.currentTier = currentTier;
 }

}